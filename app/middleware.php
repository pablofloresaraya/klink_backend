<?php
declare(strict_types=1);

use App\Application\Middleware\SessionMiddleware;
use Slim\App;
//use App\Domain\Token\Token;
use Tuupola\Middleware\JwtAuthentication;
//use Tuupola\Middleware\HttpBasicAuthentication;
use Tuupola\Middleware\CorsMiddleware;
//use Psr\Http\Message\ResponseInterface as Response;

return function (App $app) {
    $app->add(SessionMiddleware::class);
    
    //$logger = $app->get("LoggerInterface");

    $app->add(new JwtAuthentication([
        "attribute" => "jwt",
        "secure" => false, //true cuando el servidor es HTTPS
        "ignore" => ["/token"],
        "secret" => "supersecretkeyyoushouldnotcommittogithub",
        //"logger" => $logger,
        "error" => function ($response, $arguments) {
            $data["status"] = "error";
            $data["message"] = $arguments["message"];
            return $response
                ->withHeader("Content-Type", "application/json")
                ->getBody()->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        },
    ]));
    
    $app->add(new CorsMiddleware([
        "origin" => ["http://localhost:3000"],
        //"origin.server" => "http://nefo:3000",
        "methods" => ["GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"],
        "headers.allow" => ["Authorization", "If-Match", "If-Unmodified-Since","x-requested-with", "Content-Type", "origin", "accept", "client-security-token"],
        "headers.expose" => ["Authorization", "Etag"],
        "credentials" => true,
        "cache" => 7200,
        //"logger" => $logger
    ]));
    
};
