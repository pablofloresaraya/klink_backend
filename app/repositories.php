<?php
declare(strict_types=1);

use App\Domain\User\UserRepository;
use App\Domain\Contabilidad\ContabilidadRepository;
use App\Domain\Engine\EngineRepository;
use App\Domain\Token\Token;
use App\Infrastructure\Persistence\User\InMemoryUserRepository;
use App\Infrastructure\Persistence\Connection\ConnectionDataBasePostgreSql;
//use App\Infrastructure\Persistence\Connection\ConnectionDataBaseKlinik;
use App\Infrastructure\Persistence\ContabilidadSql;
use App\Infrastructure\Persistence\EngineSql;
use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;
//use Dotenv\Dotenv;

return function (ContainerBuilder $containerBuilder) {
    // Here we map our UserRepository interface to its in memory implementation
    $containerBuilder->addDefinitions([
        UserRepository::class => \DI\autowire(InMemoryUserRepository::class),
        ContabilidadRepository::class => \DI\autowire(ContabilidadSql::class),
        EngineRepository::class => \DI\autowire(EngineSql::class),
    ]);
    
    $containerBuilder->addDefinitions([
        DataBase::class => function (ContainerInterface $c) {

            $logger = $c->get('LoggerInterface');

            $conn = new ConnectionDataBasePostgreSql($logger);

            return $conn;
        },
    ]);
    
    $containerBuilder->addDefinitions([
        token::class => function (ContainerInterface $c) {
            return new Token([]);
        },
    ]);
};
