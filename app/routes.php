<?php
declare(strict_types=1);

use App\Application\Actions\User\ListUsersAction;
use App\Application\Actions\User\ViewUserAction;

use App\Application\Actions\Engine\EngineActionModules;
use App\Application\Actions\Engine\EngineActionModuleAll;
use App\Application\Actions\Engine\EngineActionModuleSections;
use App\Application\Actions\Engine\EngineActionModuleSectionMenu;

use App\Application\Actions\Patient\ListAllPatientsAction;
use App\Application\Actions\Patient\ViewPatientAction;

use App\Application\Actions\VoucherType\VoucherTypeActionListAll;
use App\Application\Actions\VoucherType\VoucherTypeActionView;
use App\Application\Actions\VoucherType\VoucherTypeActionPut;

use App\Application\Actions\Account\AccountActionListAll;

use App\Application\Actions\Account\CitiesActionListAll;
use App\Application\Actions\Account\RutActionList;
use App\Application\Actions\Account\SaveActionListAll;
use App\Application\Actions\Account\DeleteActionListAll;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

//Para generar el token
use Dotenv\Dotenv;
use Firebase\JWT\JWT;
use Tuupola\Base62;
//Fin

return function (App $app) {

    $app->get('/', function (Request $request, Response $response) {
        //$response->getBody()->write('Hello world!');
        return $response;
    });

    $app->group('/users', function (Group $group) {
        $group->get('', ListUsersAction::class);
        $group->get('/{id}', ViewUserAction::class);
    });

    $app->group('/patients', function (Group $group) {
        $group->get('', ListAllPatientsAction::class);
        $group->options('',function(Request $request, Response $response): Response {
            return $response;
        });
        $group->get('/{id}', ViewPatientAction::class);
        $group->options('/{id}',function(Request $request, Response $response): Response {
            return $response;
        });
    });

    $app->group('/modules', function (Group $group) {
        $group->get('', EngineActionModules::class);
        $group->options('',function(Request $request, Response $response): Response {
            return $response;
        });

        $group->get('/{id}/all', EngineActionModuleAll::class);
        $group->options('/{id}/all',function(Request $request, Response $response): Response {
            return $response;
        });

        $group->get('/{id}/sections', EngineActionModuleSections::class);
        $group->options('/{id}/sections',function(Request $request, Response $response): Response {
            return $response;
        });

        $group->get('/{id}/applications', EngineActionModuleSectionMenu::class);
        $group->options('/{id}/applications',function(Request $request, Response $response): Response {
            return $response;
        });
    });

    $app->group('/vouchertypes', function (Group $group) {
        $group->get('', VoucherTypeActionListAll::class);
        $group->options('', VoucherTypeActionListAll::class);
        //$group->options('',function(Request $request, Response $response): Response {
        //    return $response;
        //});
        $group->get('/{id}', VoucherTypeActionView::class);
        $group->options('/{id}',function(Request $request, Response $response): Response {
            return $response;
        });
        $group->post('/{id}/update', VoucherTypeActionPut::class);
        $group->options('/{id}/update', VoucherTypeActionPut::class);
        /*
        $group->options('/{id}/update',function(Request $request, Response $response): Response {
            return $response;
        });
        */
    });

    $app->group('/accounts', function (Group $group) {
        $group->get('', AccountActionListAll::class);
        $group->options('', AccountActionListAll::class);

        $group->get('/cities', CitiesActionListAll::class);
        $group->options('/cities', CitiesActionListAll::class);

        $group->post('/proveedor', SaveActionListAll::class);
        $group->options('/proveedor', SaveActionListAll::class);

        $group->get('/{rut}', RutActionList::class);
        $group->options('/{rut}', RutActionList::class);

        $group->put('/delete/{id}', DeleteActionListAll::class);
        $group->options('/delete/{id}', DeleteActionListAll::class);

        //$group->get('/{id}', AccountView::class);
        //$group->options('/{id}',AccountView::class);

        //$group->post('/{id}/update', AccountPut::class);
        //$group->options('/{id}/update', AccountPut::class);
    });

    /*$app->group('/cities', function (Group $group) {
        $group->get('', CitiesActionListAll::class);
        $group->options('', CitiesActionListAll::class);
    });

    $app->group('/proveedor', function (Group $group) {
        $group->get('/{rut}', RutActionList::class);
        $group->options('/{rut}', RutActionList::class);
    });*/

    /*$app->group('/proveedor', function (Group $group) {
        $group->post('', RutActionList::class);
        $group->options('', RutActionList::class);
    });*/

    $app->group('/token', function (Group $group) {

        $group->options('',function(Request $request, Response $response): Response {
            return $response;
        });
        /*{
            $response->withStatus(200)
                ->withHeader('Access-Control-Allow-Origin', 'http://nefo:3000')
                //->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
                ->withHeader('Access-Control-Allow-Credentials', 'true')
                ->getBody()->write($response);
        });
        */
        $group->get('', function (Request $request, Response $response) {

            $dotenv = Dotenv::createImmutable(__DIR__ . "/../");
            $dotenv->load();

            $requested_scopes = $request->getParsedBody() ?: [];

            //Estos son los privilegios que puede tener un usuario
            $valid_scopes = [
                "create", "write", "read", "update",
                "delete", "list", "all"
            ];

            $scopes = array_filter($requested_scopes, function ($needle) use ($valid_scopes) {
                return in_array($needle, $valid_scopes);
            });

            $now = new DateTime();
            $future = new DateTime("now +2 hours");
            $server = $request->getServerParams();

            $jti = (new Base62)->encode(random_bytes(16));

            $payload = [
                "iat" => $now->getTimeStamp(),
                "exp" => $future->getTimeStamp(),
                "jti" => $jti,
                //"sub" => $server["PHP_AUTH_USER"],
                "scope" => $scopes
            ];

            $secret = getenv("JWT_SECRET");
            $token = JWT::encode($payload, $secret, "HS256");

            $data["token"] = $token;
            $data["expires"] = $future->getTimeStamp();
            $data["name"] = 'Administrador';

            $response
                ->withStatus(201)
                ->withHeader("Content-Type", "application/json")
                ->withHeader('Access-Control-Allow-Origin', 'http://localhost:3000')
                //->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
                ->withHeader('Access-Control-Allow-Credentials', 'true')

                ->getBody()->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

            return $response;
        });
    });
};
