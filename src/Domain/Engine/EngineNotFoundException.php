<?php
declare(strict_types=1);

namespace App\Domain\Engine;

use App\Domain\DomainException\DomainRecordNotFoundException;

class EngineNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'Ocurrió un error al procesar la transacción.';
}
