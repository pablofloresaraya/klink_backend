<?php
declare(strict_types=1);

namespace App\Domain\Engine;

use JsonSerializable;

class Engine implements JsonSerializable{

    public function __construct(){}

    public function getModules(array $dataset): array{ 
        $result = $dataset;
        return $result;
    }

    public function getModuleAll(array $dataset): array{ 
        $result = $dataset;
        return $result;
    }

    public function getModuleSections(array $dataset): array{ 
        $result = $dataset;
        return $result;
    }

    public function getModuleSectionMenu(array $dataset): array{ 
        $result = $dataset;
        return $result;
    }
    
    public function jsonSerialize()
    {
        return [
            
        ];
    }
}