<?php
declare(strict_types=1);

namespace App\Domain\Engine;

use App\Infrastructure\Persistence\EngineSql;

interface EngineRepository
{

    /**
     * @param int $id
     * @return Engine
     * @throws EngineNotFoundException
     */
    public function getModules(string $p_type): array;
    public function getModuleAll(int $p_module): array;
    public function getModuleSections(int $p_module): array;
    public function getModuleSectionMenu(int $p_module, int $p_section): array;
}
