<?php
declare(strict_types=1);

namespace App\Domain\Contabilidad;

use App\Domain\DomainException\DomainRecordNotFoundException;

class ContabilidadNotFoundException extends DomainRecordNotFoundException
{
    public $message = 'Ocurrió un error al procesar la transacción.';
}
