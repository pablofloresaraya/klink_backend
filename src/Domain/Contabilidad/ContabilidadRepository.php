<?php
declare(strict_types=1);

namespace App\Domain\Contabilidad;

use App\Infrastructure\Persistence\ContabilidadSql;

interface ContabilidadRepository
{

    /**
     * @param int $id
     * @return Contabilidad
     * @throws ContabilidadNotFoundException
     */
    public function accVoucherTypeListAll(int $p_company_id): array;
    public function accVoucherTypeView(int $p_id): array;
    public function accVoucherTypeUpdate(int $p_id, int $company_id, string $voucher_type_code, string $name, string $description, string $class_type): bool;
    public function accVoucherTypeDelete(int $p_id): array;
    public function accAccountListAll(int $p_company_id, string $p_nombre): array;
    public function accRutTypeListAll(): array;
    public function accCommunesListAll(): array;
    public function accCitiesListAll(): array;
    public function accGetProv(string $p_rut): array;
    public function accValidaProv(string $p_rut): array;
    public function accSaveProv(string $p_identifier, string $p_dv, string $p_name, int $p_city, string $p_direccion, string $p_phone, string $p_mobile, string $p_email): array;
    public function accUpdateProv(string $p_identifier,string $p_name,int $p_city, string $p_direccion, string $p_phone,string $p_mobile,string $p_email, int $p_id): array;
    public function accGetIdPartner(string $p_identifier): array;
    public function accSaveCustomer(int $p_partner): array;
    public function accSaveSupplier(int $p_partner): array;
    public function accDeleteSupplier(int $p_id): array;
    public function accDeleteCustomer(int $p_id): array;
    public function accValidateSupplier(int $p_id): array;
    public function accValidateCustomer(int $p_id): array;
}
