<?php
declare(strict_types=1);

namespace App\Domain\Contabilidad;

use JsonSerializable;

class Contabilidad implements JsonSerializable{

    public function __construct(){}

    public function accVoucherTypeListAll(array $dataset): array{ 
        $result = $dataset;
        return $result;
    }

    public function accVoucherTypeView(array $dataset): array{ 
        $result = $dataset;
        return $result;
    }

    public function accVoucherTypeUpdate(array $originData, array $formData): array{
        $id = $formData['key1'];
        $data["company_id"] = $formData['key2'];
        $data["voucher_type_code"] = $formData['key3'];
        $data["name"] = $formData['key4'];
        $data["description"] = $formData['key5'];
        $data["class_type"] = $formData['key6'];

        if (!isset($id) || empty($id) || count($originData)==0) {
            throw new ContabilidadNotFoundException();
        }

        $dataset = $originData;
        
        if($originData['company_id']!=$data["company_id"]){
            $dataset['company_id'] = $data["company_id"];
        }        
        if($originData['voucher_type_code']!=$data["voucher_type_code"]){
            $dataset['voucher_type_code'] = $data["voucher_type_code"];
        }        
        if($originData['name']!=$data["name"]){
            $dataset['name'] = $data["name"];
        }        
        if($originData['description']!=$data["description"]){
            $dataset['description'] = $data["description"];
        }        
        if($originData['class_type']!=$data["class_type"]){
            $dataset['class_type'] = $data["class_type"];
        }
        
        return  array($dataset['id'], $dataset['company_id'], $dataset['voucher_type_code'], $dataset['name'], $dataset['description'], $dataset['class_type']);
    }

    public function accAccountListAll(array $dataset): array{ 
        $result = $this->accAccountCreateTree($dataset,0);
        return $result;
    }

    public function accAccountCreateTree(array $dataset,int $p_parent_id): array{
        $row = 0;
        $data = array();
        for($i=0;$i<count($dataset);$i++){
            if(empty($dataset[$i]['parent_id']) && $p_parent_id==0){
                $data[$row] = $dataset[$i];
                //$data[$row]['text'] = substr($dataset[$i]['name'],0,80);
                $data[$row]['text'] = $dataset[$i]['name'];
                $data[$row]['text_large'] = $dataset[$i]['name'];
                $data[$row]['children'] = $this->accAccountCreateTree($dataset,$dataset[$i]['id']);
                $row++;
            }
            else if($dataset[$i]['parent_id']==$p_parent_id){
                $data[$row] = $dataset[$i];
                //$data[$row]['text'] = substr($dataset[$i]['name'],0,80);
                $data[$row]['text'] = $dataset[$i]['name'];
                $data[$row]['text_large'] = $dataset[$i]['name'];

                if($dataset[$i]['type_name']=='group'){
                    $data[$row]['children'] = $this->accAccountCreateTree($dataset,$dataset[$i]['id']);
                    if(count($data[$row]['children'])==0){
                        $data[$row]['children'] = array(array('id'=>0, 'text'=>'Ninguna', 'isLeaf'=>true));
                    }
                }                    
                else
                    $data[$row]['isLeaf'] = true;

                $row++;
            }
        }
        return $data;
    }

    public function accRutTypeListAll(array $dataset): array{ 
        $result = $dataset;
        return $result;
    }

    public function accCitiesListAll(array $dataset): array{ 
        $result = $dataset;
        return $result;
    }

    public function accCommunesListAll(array $dataset): array{ 
        $result = $dataset;
        return $result;
    }

    public function accGetProv(array $dataset): array{ 
        $result = $dataset;
        return $result;
    }

    public function accValidateSupplier(array $dataset): array{ 
        $result = $dataset;
        return $result;
    }

    public function accValidateCustomer(array $dataset): array{ 
        $result = $dataset;
        return $result;
    }

    public function jsonSerialize()
    {
        return [
            
        ];
    }
    
}