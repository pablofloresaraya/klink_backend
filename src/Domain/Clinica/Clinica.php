<?php
declare(strict_types=1);

namespace App\Domain\Clinica;

use JsonSerializable;
use App\Infrastructure\Persistence\ClinicaSql;

class Clinica implements JsonSerializable
{
    private $clinica;

    public function __construct($connection)
    {
        $this->clinica = new ClinicaSql($connection);
    }

    public function getPacientes(){        
        //$conn = $this->connection; //$this->container->get(PDO::class);
        //$sql = "SELECT pc_cod, pc_nombre, pc_apellido FROM pacientes WHERE pc_cod='15572966-k'";
        //$result = $conn->query($sql);
        $result = $this->clinica->getPacientes();
        $data = array();
        $i=0;
        foreach ($result as $row) {
            $data[$i] = $row;
            $i++;
        }

        return $data;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
        ];
    }
}
