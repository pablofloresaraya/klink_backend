<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence;
use App\Domain\Engine\EngineRepository;
use App\Infrastructure\Persistence\Connection\ConnectionDataBaseKlinik;

class EngineSql implements EngineRepository
{
    private $dataBase;
    private $connection;

    public function __construct()
    {
        $this->dataBase = new ConnectionDataBaseKlinik();
    }

    public function getModules(string $p_type): array{
        $sql = "select id,name,description,is_active,parent_id,module_id,menu_type,menu_order
                from base_ui_menu
                where parent_id is null
                and menu_type=:p_type
                and is_active=true
                order by menu_order";

        $param = array(':p_type' => $p_type);
        $data = $this->dataBase->execQueryParamV2("SELECT",$sql,$param);
        return $data;
    }

    public function getModuleAll(int $p_module): array{
        $sql = "select id,name,description,is_active,parent_id,module_id,menu_type,menu_order
                from base_ui_menu
                where module_id=:p_module
                and is_active=true
                and parent_id is not null
                order by menu_order";

        $param = array(':p_module' => $p_module);
        $data = $this->dataBase->execQueryParamV2("SELECT",$sql,$param);
        return $data;
    }

    public function getModuleSections(int $p_module): array{
        $sql = "select id,name,description,is_active,parent_id,module_id,menu_type,menu_order
                from base_ui_menu
                where module_id=:p_module
                and parent_id=:p_module
                and is_active=true
                order by menu_order";

        $param = array(':p_module' => $p_module);
        $data = $this->dataBase->execQueryParamV2("SELECT",$sql,$param);
        return $data;
    }

    public function getModuleSectionMenu(int $p_module, int $p_section): array{
        $sql = "select id,name,description,is_active,parent_id,module_id,menu_type,menu_order
                from base_ui_menu
                where module_id=:p_module
                and parent_id=:p_section
                and is_active=true
                order by menu_order";

        $param = array(':p_module' => $p_module, ':p_section' => $p_section);
        $data = $this->dataBase->execQueryParamV2("SELECT",$sql,$param);
        return $data;
    }
}
