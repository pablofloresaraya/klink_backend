<?php

class ConnectionDataBasePostgreSql{

    protected $db;

    public function __construct(){
        $dbSettings['dbname'] = getenv("DB_NAME");
        $dbSettings['user'] = getenv("DB_USER");
        $dbSettings['pass'] = getenv("DB_PASSWORD");
        $dbSettings['host'] = getenv("HOST");

        $conn = NULL;
        $dbSettings['host'] = 'localhost';
        $dbSettings['dbname'] = 'clinica';
        $dbSettings['user'] = 'postgres';
        $dbSettings['pass'] = 'abvbic';

        try{
            $pdo = new PDO("pgsql:host=" . $dbSettings['host'] . ";dbname=" . $dbSettings['dbname'], $dbSettings['user'], $dbSettings['pass']);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);            
        }
        catch(PDOException $e){
            echo "Connection failed: ".$e->getMessage();
        }
        
        $this->db = $pdo;
    }
   
    public function getConnection(){
        return $this->db;
    }

    public function execute(){

    }
}

?>