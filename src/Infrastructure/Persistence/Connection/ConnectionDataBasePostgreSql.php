<?php
namespace App\Infrastructure\Persistence\Connection;
use Psr\Log\LoggerInterface;
use \PDO;
use Dotenv\Dotenv;

class ConnectionDataBasePostgreSql{

    protected $connection;
    protected $logger;

    //public function __construct($host,$dbname,$user,$pass,LoggerInterface $logger){
    public function __construct(LoggerInterface $logger){

        $dotenv = Dotenv::createImmutable(__DIR__ . "/../../../../");
        $dotenv->load();

        $dbSettings['dbname'] = getenv("DB_NAME");
        $dbSettings['user'] = getenv("DB_USER");
        $dbSettings['pass'] = getenv("DB_PASSWORD");
        $dbSettings['host'] = getenv("DB_HOST");
        
        $this->logger = $logger;

        try{
            $pdo = new PDO("pgsql:host=" . $dbSettings['host'] . ";dbname=" . $dbSettings['dbname'], $dbSettings['user'], $dbSettings['pass']);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);            
            $this->logger->info("Connection Ok");
        }
        catch(PDOException $e){
            $this->logger->info("Connection failed: ".$e->getMessage());
            //echo "Connection failed: ".$e->getMessage();
        }
        
        $this->connection = $pdo;
    }
   
    public function getConnection(){
        return $this->connection;
    }
    
    public function execQuery($sql){        
        $conn = $this->connection;
        $result = $conn->query($sql);
        
        $data = array();
        $i=0;
        foreach ($result as $row) {
            $data[$i] = $row;
            $i++;
        }
        return $data;
    }
    
    public function execQueryParam($sql,$param){        
        $conn = $this->connection;
        $sth = $conn->prepare($sql);
        $sth->execute($param);
        $result = $sth->fetchAll();
        return $result;
    }
}

?>