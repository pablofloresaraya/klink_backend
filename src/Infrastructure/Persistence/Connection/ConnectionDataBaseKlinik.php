<?php
namespace App\Infrastructure\Persistence\Connection;
use \PDO;
use Dotenv\Dotenv;

class ConnectionDataBaseKlinik{

    protected $connection;
    protected $logger;

    public function __construct(){

        $dotenv = Dotenv::createImmutable(__DIR__ . "/../../../../");
        $dotenv->load();

        $dbSettings['dbname'] = getenv("DB_NAME");
        $dbSettings['user'] = getenv("DB_USER");
        $dbSettings['pass'] = getenv("DB_PASSWORD");
        $dbSettings['host'] = getenv("DB_HOST");

        try{
            $pdo = new PDO("pgsql:host=" . $dbSettings['host'] . ";dbname=" . $dbSettings['dbname'], $dbSettings['user'], $dbSettings['pass']);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);            
            //$this->logger->info("Connection Ok");
        }
        catch(PDOException $e){
            //$this->logger->info("Connection failed: ".$e->getMessage());
            echo "Connection failed: ".$e->getMessage();
        }
        
        $this->connection = $pdo;
    }
   
    public function getConnection(){
        return $this->connection;
    }
    
    public function execQuery($sql){        
        $conn = $this->connection;
        $result = $conn->query($sql);
        
        $data = array();
        $i=0;
        foreach ($result as $row) {
            $data[$i] = $row;
            $i++;
        }
        return $data;
    }
    
    public function execQueryParam($sql,$param){        
        $conn = $this->connection;
        $sth = $conn->prepare($sql);
        $sth->execute($param);
        $result = $sth->fetchAll();
        return $result;
    }
    
    public function execQueryParamV2($type,$sql,$param){        
        $conn = $this->connection;
        $sth = $conn->prepare($sql);
        if($sth->execute($param)){
            if($type=="SELECT"){            
                $result = $sth->fetchAll();
                return $result;
            }
            else{
                return true;
            }
        }
        else{
            return false;
        }
    }
}

?>