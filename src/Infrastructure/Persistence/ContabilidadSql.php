<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence;

use App\Domain\Contabilidad\Contabilidad;
use App\Domain\Contabilidad\ContabilidadNotFoundException;
use App\Domain\Contabilidad\ContabilidadRepository;
use App\Infrastructure\Persistence\Connection\ConnectionDataBaseKlinik;


class ContabilidadSql implements ContabilidadRepository
{
    private $dataBase;

    public function __construct(){
        $this->dataBase = new ConnectionDataBaseKlinik();
    }

    //Acc_VoucherType
    public function accVoucherTypeListAll(int $p_company_id): array{        
        
        $sql = "select id, company_id, voucher_type_code, name, description, class_type, create_uid, create_date
                from acc_voucher_type as a
                where company_id=:p_company_id
                order by id;";
                
        $param = array(':p_company_id' => $p_company_id);
        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;      
    }

    public function accVoucherTypeView(int $p_id): array{        
        $sql = "SELECT * FROM acc_voucher_type WHERE id=:p_id";
        //$data = $this->dataBase->executeQuery($sql);
        $param = array(':p_id' => $p_id);
        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;
    }

    public function accVoucherTypeUpdate(int $p_id, int $p_company_id, string $p_voucher_type_code, string $p_name, string $p_description, string $p_class_type): bool{        
        $sql = "UPDATE acc_voucher_type 
                SET company_id=:p_company_id, voucher_type_code=:p_voucher_type_code, name=:p_name, description=:p_description, class_type=:p_class_type
                WHERE id=:p_id";
        
        $param = array(':p_id' => $p_id,':p_company_id' => $p_company_id,':p_voucher_type_code' => $p_voucher_type_code,':p_name' => $p_name,':p_description' => $p_description,':p_class_type' => $p_class_type);
        $result = $this->dataBase->execQueryParamV2("UPDATE",$sql,$param);
        return $result;
    }

    public function accVoucherTypeDelete(int $p_id): array{        
        $sql = "DELETE
                FROM acc_voucher_type 
                WHERE id=:p_id";
        
        $param = array(':p_id' => $p_id);
        $data = $this->dataBase->execQueryParamV2("DELETE",$sql,$param);
        return $data;
    }

    //Acc_Account
    public function accAccountListAll(int $p_company_id,string $p_nombre): array{     
        $sql = "select  regexp_matches(upper(a.name),upper(:p_nombre))
                    ,a.*,b.name type_name
                from acc_account as a
                left outer join acc_account_type as b on b.id=a.type_id
                where 1=1
                and a.company_id=:p_company_id
                order by a.account_code;";
                
        $param = array(':p_nombre' => $p_nombre, ':p_company_id' => $p_company_id);
        $data = $this->dataBase->execQueryParamV2("SELECT",$sql,$param);
        return $data;   
    }

    public function accRutTypeListAll(): array{        
        
        $sql = "select a.id, a.identifier as run, a.identifier_dv, (a.identifier||'-'||a.identifier_dv) as rut, a.name from rh_partner a
                    where ( 0<(select count(*) from arc_customer where partner_id=a.id and activo=TRUE)
                            or 0<(select count(*) from aps_supplier where partner_id=a.id and activo=TRUE) )";
                
        $param = array();
        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;      
    }

    public function accCommunesListAll(): array{        
        
        $sql = "select id, country_id, name, code from base_territorial where parent_id IS NULL order by id ASC";
                
        $param = array();
        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;      
    }

    public function accCitiesListAll(): array{        
        
        $sql = "select id, country_id, name, parent_id, code from base_territorial where parent_id is not NULL";
                   
        $param = array();
        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;      
    }

    public function accGetProv(string $p_rut): array{        
        
        $sql = "select a.*
                ,(select name from base_territorial where id = a.address_city_id) as name_ciudad
                ,(select parent_id from base_territorial where id = a.address_city_id) as id_comuna
                ,(select name from base_territorial where id = (select parent_id from base_territorial where id = a.address_city_id)) as name_comuna
                ,(select count(*) from arc_customer where partner_id=a.id and activo=TRUE) as customer
                ,(select count(*) from aps_supplier where partner_id=a.id and activo=TRUE) as supplier
        from rh_partner a 
            where a.identifier=:p_rut";
                   
        $param = array(':p_rut' => $p_rut);
        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;      
    }

    public function accValidaProv(string $p_rut): array{        
        
        $sql = "select count(*) as valida from rh_partner where identifier=:p_rut";
                   
        $param = array(':p_rut' => $p_rut);
        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;      
    }

    public function accSaveProv(
                                string $p_identifier,
                                string $p_dv,
                                string $p_name,                                
                                int $p_city,                               
                                string $p_direccion,
                                string $p_phone,
                                string $p_mobile,
                                string $p_email                                
                            ): array{        
        
        $sql = "insert into rh_partner
                (   identifier, 
                    identifier_dv, 
                    name,
                    user_id, 
                    address_country_id, 
                    address_city_id,
                    address,
                    phone,
                    mobile_phone,
                    email
                )
                values (
                    :p_identifier,
                    :p_dv,
                    :p_name,
                    1,
                    (select country_id from base_territorial where id = :p_city),
                    :p_city,
                    :p_direccion,
                    :p_phone,
                    :p_mobile,
                    :p_email                   
                )";
                   
        $param = array(
                        ':p_identifier' => $p_identifier,
                        ':p_dv' => $p_dv,
                        ':p_name' => $p_name,                        
                        ':p_city' => $p_city,
                        ':p_direccion' => $p_direccion,
                        ':p_phone' => $p_phone,
                        ':p_mobile' => $p_mobile,
                        ':p_email' => $p_email                         
                    );
        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;      
    }

    public function accGetIdPartner(string $p_identifier): array{                                           
          
        $sql = "select id from rh_partner where identifier=:p_identifier";

        $param = array(':p_identifier' => $p_identifier);

        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;

    }

    public function accSaveCustomer(int $p_partner): array{                                           
          
        $sql = "insert into arc_customer (partner_id, activo) values (:p_partner, true)";

        $param = array(':p_partner' => $p_partner);

        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;

    }

    public function accSaveSupplier(int $p_partner): array{                                           
          
        $sql = "insert into aps_supplier (partner_id, activo) values (:p_partner, true)";

        $param = array(':p_partner' => $p_partner);

        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;

    }

    public function accUpdateProv(
                                    string $p_identifier,                                   
                                    string $p_name,
                                    int $p_city,
                                    string $p_direccion,
                                    string $p_phone,
                                    string $p_mobile,
                                    string $p_email,
                                    int $p_id
                                ): array{        

        $sql = "update rh_partner a 
                set 
                    identifier = :p_identifier,                
                    name = :p_name, 
                    address_country_id = (select country_id from base_territorial where id = :p_city), 
                    address_city_id = :p_city,
                    address = :p_direccion,
                    phone = :p_phone,
                    mobile_phone = :p_mobile,
                    email = :p_email
                where id = :p_id and user_id = 1";

        $param = array(
            ':p_identifier' => $p_identifier,
            ':p_name' => $p_name,
            ':p_city' => $p_city,
            ':p_direccion' => $p_direccion,
            ':p_phone' => $p_phone,
            ':p_mobile' => $p_mobile,
            ':p_email' => $p_email,
            ':p_id' => $p_id
        );

        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;      
    }

    public function accValidateSupplier(int $p_id): array{                                           
          
        $sql = "select count(*) as supplier from aps_supplier where partner_id=:p_id";

        $param = array(':p_id' => $p_id);

        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;

    }

    public function accDeleteSupplier(int $p_id): array{                                           
          
        $sql = "update aps_supplier set activo=false where partner_id=:p_id";

        $param = array(':p_id' => $p_id);

        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;

    }

    public function accValidateCustomer(int $p_id): array{                                           
          
        $sql = "select count(*) as customer from arc_customer where partner_id=:p_id";

        $param = array(':p_id' => $p_id);

        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;

    }

    public function accDeleteCustomer(int $p_id): array{                                           
          
        $sql = "update arc_customer set activo=false where partner_id=:p_id";

        $param = array(':p_id' => $p_id);

        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;

    }

}