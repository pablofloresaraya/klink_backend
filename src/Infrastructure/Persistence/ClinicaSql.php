<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence;

class ClinicaSql
{
    private $dataBase;
    private $connection;
    /*
    public function __construct($connection)
    {
        $this->connection = $connection;
    }
    */
    public function __construct($dataBase)
    {
        $this->dataBase = $dataBase;
        $this->connection = $this->dataBase->getConnection();
    }

    public function getPacientes(int $p_limit,string $p_nombre,string $p_apellido){        
        //$sql = "SELECT * FROM pacientes LIMIT :limit";
        $sql = "select   regexp_matches(upper(pc_nombre),upper(:p_nombre))
                        ,regexp_matches(upper(pc_apellido),upper(:p_apellido))
                        ,pac.pc_cod,pac.pc_nombre,pac.pc_apellido,pac.pc_nombre||', '||pac.pc_apellido as nombre
                from pacientes as pac
                where 1=1
                order by nombre
                LIMIT :p_limit;";
                
        $param = array(':p_limit' => $p_limit, ':p_nombre' => $p_nombre, ':p_apellido' => $p_apellido);
        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;
    }

    public function getPaciente(string $p_pc_cod){        
        $sql = "SELECT * FROM pacientes WHERE pc_cod=:p_pc_cod";
        //$data = $this->dataBase->executeQuery($sql);
        $param = array(':p_pc_cod' => $p_pc_cod);
        $data = $this->dataBase->execQueryParam($sql,$param);
        return $data;
    }
}
