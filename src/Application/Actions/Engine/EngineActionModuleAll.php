<?php
declare(strict_types=1);

namespace App\Application\Actions\Engine;

use Psr\Http\Message\ResponseInterface as Response;

class EngineActionModuleAll extends EngineAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $var1 = (int) $this->resolveArg('id');
        $var1 = (empty($var1)) ? -1 : $var1;
        
        $data = array();

        $result = $this->engineRepository->getModuleAll($var1);
        $data = $this->engine->getModuleAll($result);

        $this->logger->info("EngineActionModules: Ok");
        return $this->respondWithData($data);     
    }
}
