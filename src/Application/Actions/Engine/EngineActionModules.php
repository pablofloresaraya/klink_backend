<?php
declare(strict_types=1);

namespace App\Application\Actions\Engine;

use Psr\Http\Message\ResponseInterface as Response;

class EngineActionModules extends EngineAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $result = $this->engineRepository->getModules("module");
        $data = $this->engine->getModules($result);

        $this->logger->info("EngineActionModules: Ok");
        return $this->respondWithData($data);     
    }
}
