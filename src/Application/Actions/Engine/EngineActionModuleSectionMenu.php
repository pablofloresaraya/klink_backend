<?php
declare(strict_types=1);

namespace App\Application\Actions\Engine;

use Psr\Http\Message\ResponseInterface as Response;

class EngineActionModuleSectionMenu extends EngineAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $queryParam = $this->request->getQueryParams();
        $moduleId = (int) $queryParam["module_id"];
        $sectionId = (int) $queryParam["section_id"];
        $moduleId = (empty($moduleId)) ? '' : $moduleId;
        $sectionId = (empty($sectionId)) ? '' : $sectionId;
        
        $data = array();
        if($moduleId!='' && $sectionId!=''){
            $result = $this->engineRepository->getModuleSectionMenu($moduleId, $sectionId);
            $data = $this->engine->getModuleSectionMenu($result);
        }

        $this->logger->info("ListAllApplicationsAction: Ok");
        return $this->respondWithData($data);     
    }
}
