<?php
declare(strict_types=1);

namespace App\Application\Actions\Engine;

use App\Application\Actions\Action;
use App\Domain\Engine\EngineRepository;
use App\Domain\Engine\Engine;
use Psr\Log\LoggerInterface;

abstract class EngineAction extends Action
{
    /**
     * @var DataBase
     */
    protected $dataBase;
    
    /**
     * @var engine
     */
    protected $engine;

    /**
     * @param LoggerInterface $logger
     * @param EngineRepository  $engineRepository
     */
    public function __construct(LoggerInterface $logger, EngineRepository $engineRepository)
    {
        parent::__construct($logger);
        $this->logger->info("EngineAction: Ok");
        $this->engineRepository = $engineRepository;
        $this->engine = new Engine();
        //$this->contabilidad = new ContabilidadSql($dataBase);
    }
}
