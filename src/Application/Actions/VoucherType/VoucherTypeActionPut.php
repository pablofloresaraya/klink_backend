<?php
declare(strict_types=1);

namespace App\Application\Actions\VoucherType;

use Psr\Http\Message\ResponseInterface as Response;

class VoucherTypeActionPut extends VoucherTypeAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        //var_dump($this->request);
        //$datos = json_decode(file_get_contents('php://input'));
        //print_r($datos);
        //$this->logger->info("VoucherTypeActionPut file_get_contents: ".file_get_contents('php://input'));
        //$this->logger->info("VoucherTypeActionPut getAttribute: ".$this->request->getAttribute("key"));
        //$this->logger->info("VoucherTypeActionPut getParsedBody: ".$this->request->getParsedBody());
        //$this->logger->info("VoucherTypeActionPut getQueryParams: ".print_r($this->request->getQueryParams()));
        $formData = $this->request->getParsedBody();
        //$p_id = $formData["key1"];
        //var_dump($p_id);
        //var_dump(json_decode(json_encode($this->request->getParsedBody())));
        //$formData = $this->getFormData();
        //print_r($formData);
        //$queryParam = $this->request->getQueryParams();
        //$formData = $queryParam;
        $p_id = (int) $formData["key1"];
        $originData = $this->contabilidadRepository->accVoucherTypeView($p_id);

        list($p_id, $p_company_id, $p_voucher_type_code, $p_name, $p_description, $p_class_type) = $this->contabilidad->accVoucherTypeUpdate($originData[0], $formData);

        $result = $this->contabilidadRepository->accVoucherTypeUpdate((int) $p_id,(int) $p_company_id,(string) $p_voucher_type_code,(string) $p_name,(string) $p_description,(string) $p_class_type);
        if(!$result){
            $data["estado"]=0;
            $data["mensaje"]="No fue posible realizar la actualización";
        }
        else{
            $data["estado"]=1;
            $data["mensaje"]="Los datos fueron actualizado correctamente";
        }
        
        $this->logger->info("VoucherType of id `${p_id}` was modified.");

        return $this->respondWithData($data);
    }
}
