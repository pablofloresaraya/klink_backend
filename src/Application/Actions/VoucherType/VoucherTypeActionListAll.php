<?php
declare(strict_types=1);

namespace App\Application\Actions\VoucherType;

use Psr\Http\Message\ResponseInterface as Response;

class VoucherTypeActionListAll extends VoucherTypeAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $queryParam = $this->request->getQueryParams();
        $var1 = (int) $queryParam["var1"];
        $var1 = (empty($var1)) ? -1 : $var1;
        
        $data = array();
        //$data = $this->contabilidadRepository->getAllAccVoucherType($var1);
        
        $result = $this->contabilidadRepository->accVoucherTypeListAll($var1);
        $data = $this->contabilidad->accVoucherTypeListAll($result);
        //$data = $this->contabilidad->getAccVoucherType($limit,$p_filtro1);
        $this->logger->info("ListVoucherTypeAction: Ok");
        return $this->respondWithData($data);     
    }
}
