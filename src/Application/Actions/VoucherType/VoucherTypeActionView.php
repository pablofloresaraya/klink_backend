<?php
declare(strict_types=1);

namespace App\Application\Actions\VoucherType;

use Psr\Http\Message\ResponseInterface as Response;

class VoucherTypeActionView extends VoucherTypeAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $id = (int) $this->resolveArg('id');
        $result = $this->contabilidadRepository->accVoucherTypeView($id);
        $data = $this->contabilidad->accVoucherTypeView($result);
        $this->logger->info("VoucherType of id `${id}` was viewed.");

        return $this->respondWithData($data);
    }
}
