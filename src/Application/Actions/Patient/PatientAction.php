<?php
declare(strict_types=1);

namespace App\Application\Actions\Patient;

use App\Application\Actions\Action;
use Psr\Log\LoggerInterface;
use App\Infrastructure\Persistence\Connection\ConnectionDataBasePostgreSql as DataBase;
use App\Infrastructure\Persistence\ClinicaSql as ClinicaSql;

abstract class PatientAction extends Action
{
    /**
     * @var DataBase
     */
    protected $dataBase;
    
    /**
     * @var clinica
     */
    protected $clinica;

    /**
     * @param LoggerInterface $logger
     * @param DataBase  $dataBase
     */
    public function __construct(LoggerInterface $logger, DataBase $dataBase)
    {
        parent::__construct($logger);
        $this->dataBase = $dataBase;
        $this->clinica = new ClinicaSql($dataBase);
    }
}
