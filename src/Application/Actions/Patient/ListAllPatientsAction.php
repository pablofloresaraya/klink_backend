<?php
declare(strict_types=1);

namespace App\Application\Actions\Patient;

use Psr\Http\Message\ResponseInterface as Response;

class ListAllPatientsAction extends PatientAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $queryParam = $this->request->getQueryParams();
        $limit = (int) $queryParam["limit"];
        $limit = (empty($limit)) ? 0 : $limit;
        $p_nombre = (string) $queryParam["nombre"];
        $p_nombre = (empty($p_nombre)) ? '' : $p_nombre;
        $p_apellido = (string) $queryParam["apellido"];
        $p_apellido = (empty($p_apellido)) ? '' : $p_apellido;
        
        $data = array();
        if($p_nombre!='' || $p_apellido!=''){
            $data = $this->clinica->getPacientes($limit,$p_nombre,$p_apellido);
        }

        $this->logger->info("ListAllPatientsAction: Ok");
        return $this->respondWithData($data);     
    }
}
