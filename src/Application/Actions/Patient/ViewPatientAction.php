<?php
declare(strict_types=1);

namespace App\Application\Actions\Patient;

use Psr\Http\Message\ResponseInterface as Response;

class ViewPatientAction extends PatientAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $id = (string) $this->resolveArg('id');
        $data = $this->clinica->getPaciente($id);

        $this->logger->info("Patient of id `${id}` was viewed.");

        return $this->respondWithData($data);
    }
}
