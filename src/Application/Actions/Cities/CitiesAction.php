<?php
declare(strict_types=1);

namespace App\Application\Actions\Cities;

use App\Application\Actions\Action;
use App\Domain\Contabilidad\ContabilidadRepository;
use App\Domain\Contabilidad\Contabilidad;
use Psr\Log\LoggerInterface;
use App\Infrastructure\Persistence\Connection\ConnectionDataBasePostgreSql as DataBase;
use App\Infrastructure\Persistence\ContabilidadSql as ContabilidadSql;

abstract class CitiesAction extends Action
{
    /**
     * @var DataBase
     */
    protected $dataBase;
    
    /**
     * @var contabilidad
     */
    protected $contabilidad;

    /**
     * @param LoggerInterface $logger
     * @param DataBase  $dataBase
     */

    /**
     * @param LoggerInterface $logger
     * @param ContabilidadRepository  $contabilidadRepository
     */
    public function __construct(LoggerInterface $logger, ContabilidadRepository $contabilidadRepository)
    {
        parent::__construct($logger);
        $this->logger->info("CitiesAction: Ok");
        $this->contabilidadRepository = $contabilidadRepository;
        $this->contabilidad = new Contabilidad();
    }
}