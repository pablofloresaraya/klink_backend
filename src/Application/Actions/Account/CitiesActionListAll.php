<?php
declare(strict_types=1);

namespace App\Application\Actions\Account;

use Psr\Http\Message\ResponseInterface as Response;

class CitiesActionListAll extends CitiesAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        
        $data = array();
        $data1 = array();
        $data2 = array();
        $dataMaster = array();

        $result = $this->contabilidadRepository->accCitiesListAll();
        $data = $this->contabilidad->accCitiesListAll($result);

        $result1 = $this->contabilidadRepository->accRutTypeListAll();
        $data1 = $this->contabilidad->accRutTypeListAll($result1);

        $result2 = $this->contabilidadRepository->accCommunesListAll();
        $data2 = $this->contabilidad->accCommunesListAll($result2);

        /*$dataMaster['cities']   = $data;
        $dataMaster['ruts']     = $data1;
        $dataMaster['communes'] = $data2;*/

        $dataMaster[0] = ["cities" => $data, "ruts" => $data1, "communes" => $data2];
        
        $this->logger->info("accRutCitiesListAll: Ok");
        return $this->respondWithData($dataMaster[0]);     
    }
}