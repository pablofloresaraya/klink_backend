<?php
declare(strict_types=1);

namespace App\Application\Actions\Account;

use Psr\Http\Message\ResponseInterface as Response;

class ListAllAccountAction extends AccountAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $queryParam = $this->request->getQueryParams();
        $limit = (int) $queryParam["limit"];
        $limit = (empty($limit)) ? 0 : $limit;
        $p_filtro1 = (string) $queryParam["var1"];
        //$p_filtro1 = (empty($p_filtro1)) ? '' : $p_filtro1;
        
        $data = array();
        $data = $this->contabilidad->getAllAccAccount($limit,$p_filtro1);
        $this->logger->info("ListAllAccountAction: Ok");
        return $this->respondWithData($data);     
    }
}
