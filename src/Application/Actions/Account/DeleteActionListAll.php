<?php
declare(strict_types=1);

namespace App\Application\Actions\Account;

use Psr\Http\Message\ResponseInterface as Response;

class DeleteActionListAll extends DeleteAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {        
        //post
        $param = json_decode((string)$this->request->getBody(), true);

        $id_partner = intval($this->request->getAttribute('id'));

        $data = array();

        $check=false;

        if($param["supplier"]){

            $supplier=$this->contabilidadRepository->accValidateSupplier($id_partner);

            if($supplier[0]["supplier"]){
                $this->contabilidadRepository->accDeleteSupplier($id_partner);
            }

            $check=true;

        }

        if($param["customer"]){

            $customer=$this->contabilidadRepository->accValidateCustomer($id_partner);
            
            if($customer[0]["customer"]){
                $this->contabilidadRepository->accDeleteCustomer($id_partner);
            }

            $check=true;

        }

        //$data = $this->contabilidad->accGetProv($result);

        if($check){

            $this->logger->info("DeleteActionList: Ok");

            $clean[0]["id"]         = "";
            $clean[0]["rut"]        = "";
            $clean[0]["dig"]        = "";
            $clean[0]["cliente"]    = false;
            $clean[0]["proveedor"]  = false;
            $clean[0]["nombre"]     = "";
            $clean[0]["giro"]       = "";
            $clean[0]["direccion"]  = "";
            $clean[0]["comuna"]     = ["id" => "", "name" => ""];
            $clean[0]["ciudad"]     = ["id" => "", "name" => ""];
            $clean[0]["telefono"]   = "";
            $clean[0]["celular"]    = "";
            $clean[0]["email"]      = "";
            $clean[0]["sucursal"]   = "";
            $clean[0]["email_suc"]  = "";

            $array[0] = ["check" => $check, "clean" => $clean[0]];

        }else{

            $modal[0] = ["show" => true, "msg" => "Seleccione Proveedor y/o Cliente", "clase" => ""];
            $array[0] = ["check" => $check, "modal" => $modal[0]];
        }

        return $this->respondWithData($array[0]); 

        //post
        /*$param = json_decode((string)$this->request->getBody(), true);

        return $this->respondWithData($param);*/

    }
}