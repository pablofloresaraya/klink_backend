<?php
declare(strict_types=1);

namespace App\Application\Actions\Account;

use Psr\Http\Message\ResponseInterface as Response;

class SaveActionListAll extends SaveAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {        
        //post
        $param = json_decode((string)$this->request->getBody(), true);

        $valida = $this->contabilidadRepository->accValidaProv($param["rut"]);

        $data = array();

        $check=true;

        if($param["proveedor"] || $param["cliente"]){

            if(!$valida[0]["valida"]){ //no existe, nuevo            
                
                $this->contabilidadRepository->accSaveProv(
                                                            $param["rut"],
                                                            $param["dig"],
                                                            $param["nombre"],                                                        
                                                            $param["ciudad"]["id"],                                                        
                                                            $param["direccion"],
                                                            $param["telefono"],
                                                            $param["celular"],
                                                            $param["email"]                                                        
                                                        );
                                        
                //rescato id-partner
                $partner=$this->contabilidadRepository->accGetIdPartner($param["rut"]);                                                                                
                
                if($param["proveedor"]){
                    $this->contabilidadRepository->accSaveSupplier($partner[0]["id"]);
                }

                if($param["cliente"]){
                    $this->contabilidadRepository->accSaveCustomer($partner[0]["id"]);
                }
                
                //$data = $this->contabilidad->accSaveProv($result);
                $this->logger->info("SaveActionList: Ok");
                $msg = "La Información se ingreso con éxito";

            }else{ //existe, actualiza

                //rescato id-partner
                $partner=$this->contabilidadRepository->accGetIdPartner($param["rut"]); 

                $this->contabilidadRepository->accUpdateProv(
                    $param["rut"],
                    $param["nombre"],
                    $param["ciudad"]["id"],
                    $param["direccion"],
                    $param["telefono"],
                    $param["celular"],
                    $param["email"],
                    $partner[0]["id"]
                );
            
                $msg = "La Información se actualizó con éxito";

            }

        }else{

            $check = false;
            $msg = "Debe seleccionar cliente y/o proveedor.";

        }

        $modal[0] = ["show" => true, "msg" => $msg, "clase" => "" ];

        if($check){
        
            $clean[0]["id"]         = "";
            $clean[0]["rut"]        = "";
            $clean[0]["dig"]        = "";
            $clean[0]["cliente"]    = false;
            $clean[0]["proveedor"]  = false;
            $clean[0]["nombre"]     = "";
            $clean[0]["giro"]       = "";
            $clean[0]["direccion"]  = "";
            $clean[0]["comuna"]     = ["id" => "", "name" => ""];
            $clean[0]["ciudad"]     = ["id" => "", "name" => ""];
            $clean[0]["telefono"]   = "";
            $clean[0]["celular"]    = "";
            $clean[0]["email"]      = "";
            $clean[0]["sucursal"]   = "";
            $clean[0]["email_suc"]  = "";

            $validate[0] = [ "email" => ["show" => false, "msg" => $msg, "clase" => "" ], 
                            "emailSuc" => ["show" => false, "msg" => $msg, "clase" => "" ] 
                            ];

            $array[0] = ["check" => $check, "clean" => $clean[0], "modal" => $modal[0], "validate" => $validate[0]];

        }else{

            $array[0] = ["check" => $check, "modal" => $modal[0]];

        }

        return $this->respondWithData($array[0]);

    }
}