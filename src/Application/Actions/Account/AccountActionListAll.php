<?php
declare(strict_types=1);

namespace App\Application\Actions\Account;

use Psr\Http\Message\ResponseInterface as Response;

class AccountActionListAll extends AccountAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        
        $queryParam = $this->request->getQueryParams();
        $p_var1 = (int) $queryParam["var1"];
        $p_filtro1 = (string) $queryParam["var2"];
        
        $data = array();
        $result = $this->contabilidadRepository->accAccountListAll($p_var1,$p_filtro1);
        $data = $this->contabilidad->accAccountListAll($result);
        
        $this->logger->info("AccountActionListAll: Ok");
        return $this->respondWithData($data);     
    }
}
