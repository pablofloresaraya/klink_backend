<?php
declare(strict_types=1);

namespace App\Application\Actions\Account;

use Psr\Http\Message\ResponseInterface as Response;

class ViewAccountAction extends AccountAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $id = (string) $this->resolveArg('id');
        $data = $this->contabilidad->getAccAccount($id);

        $this->logger->info("Account of id `${id}` was viewed.");

        return $this->respondWithData($data);
    }
}
