<?php
declare(strict_types=1);

namespace App\Application\Actions\Account;

use Psr\Http\Message\ResponseInterface as Response;

class RutActionList extends RutAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {        
        $rut = (string) $this->resolveArg('rut');

        $data = array();
        $result = $this->contabilidadRepository->accGetProv($rut);
        $data = $this->contabilidad->accGetProv($result);
        $this->logger->info("RutActionList: Ok");

        $array_city[0]   = ["id" => $data[0]["address_city_id"], "name" => $data[0]["name_ciudad"]];
        $array_comuna[0] = ["id" => $data[0]["id_comuna"], "name" => $data[0]["name_comuna"]];

        $array[0]["id"]         = $data[0]["id"];
        $array[0]["rut"]        = $data[0]["identifier"];
        $array[0]["dig"]        = $data[0]["identifier_dv"];
        $array[0]["cliente"]    = $data[0]["customer"];
        $array[0]["proveedor"]  = $data[0]["supplier"];
        $array[0]["nombre"]     = $data[0]["name"];
        $array[0]["giro"]       = "";
        $array[0]["direccion"]  = $data[0]["address"];
        $array[0]["comuna"]     = $array_comuna[0];
        $array[0]["ciudad"]     = $array_city[0];
        $array[0]["telefono"]   = $data[0]["phone"];
        $array[0]["celular"]    = $data[0]["mobile_phone"];
        $array[0]["email"]      = $data[0]["email"];
        $array[0]["sucursal"]   = "";
        $array[0]["email_suc"]  = "";

        return $this->respondWithData($array[0]); 

        //post
        /*$param = json_decode((string)$this->request->getBody(), true);

        return $this->respondWithData($param);*/

    }
}