<?php
declare(strict_types=1);

namespace App\Application\Actions\Application;

use App\Application\Actions\Action;
use Psr\Log\LoggerInterface;
use App\Infrastructure\Persistence\Connection\ConnectionDataBasePostgreSql as DataBase;
use App\Infrastructure\Persistence\EngineSql as EngineSql;

abstract class ApplicationAction extends Action
{
    /**
     * @var DataBase
     */
    protected $dataBase;
    
    /**
     * @var clinica
     */
    protected $engine;

    /**
     * @param LoggerInterface $logger
     * @param DataBase  $dataBase
     */
    public function __construct(LoggerInterface $logger, DataBase $dataBase)
    {
        parent::__construct($logger);
        $this->dataBase = $dataBase;
        $this->engine = new EngineSql($dataBase);
    }
}
