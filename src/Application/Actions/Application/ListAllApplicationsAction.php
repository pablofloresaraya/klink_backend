<?php
declare(strict_types=1);

namespace App\Application\Actions\Application;

use Psr\Http\Message\ResponseInterface as Response;

class ListAllApplicationsAction extends ApplicationAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $queryParam = $this->request->getQueryParams();
        $moduleId = (int) $queryParam["p_module_id"];
        $moduleId = (empty($moduleId)) ? '' : $moduleId;
        
        $data = array();
        if($moduleId!=''){
            $data = $this->engine->getApplicationsModule($moduleId);
        }

        $this->logger->info("ListAllApplicationsAction: Ok");
        return $this->respondWithData($data);     
    }
}
