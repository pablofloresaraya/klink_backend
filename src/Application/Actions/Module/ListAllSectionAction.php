<?php
declare(strict_types=1);

namespace App\Application\Actions\Module;

use Psr\Http\Message\ResponseInterface as Response;

class ListAllSectionsAction extends ModuleAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $queryParam = $this->request->getQueryParams();
        $moduleId = (int) $queryParam["p_module_id"];
        $moduleId = (empty($moduleId)) ? '' : $moduleId;
        
        $data = array();
        if($moduleId!=''){
            $data = $this->engine->getModuleSections($moduleId);
        }

        $this->logger->info("ListAllSectionsAction: Ok");
        return $this->respondWithData($data);     
    }
}
