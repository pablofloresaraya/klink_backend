<?php
declare(strict_types=1);

namespace App\Application\Actions\Module;

use Psr\Http\Message\ResponseInterface as Response;

class ListAllUiMenuAction extends ModuleAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $queryParam = $this->request->getQueryParams();
        $moduleId = (int) $queryParam["module_id"];
        $moduleId = (empty($moduleId)) ? '' : $moduleId;

        $data = array();
        if($moduleId!=''){
            $data = $this->engine->getModuleUiMenu($moduleId);
        }

        $this->logger->info("ListAllUiMenuAction: Ok");
        return $this->respondWithData($data);
    }
}
