<?php
declare(strict_types=1);

namespace App\Application\Actions\Module;

use Psr\Http\Message\ResponseInterface as Response;

class ViewApplicationAction extends ModuleAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $queryParam = $this->request->getQueryParams();
        $moduleId = (int) $queryParam["module"];

        $id = (string) $this->resolveArg('id');
        $data = $this->engine->getApplication($moduleId, $id);

        $this->logger->info("Application of id `${id}` was viewed.");

        return $this->respondWithData($data);
    }
}
